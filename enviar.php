<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") { 
  $nome = trim($_POST["nome"]); $email = trim($_POST["email"]); $telefone = trim($_POST["telefone"]);

if ($nome == "" OR $email == "" OR $telefone == "") {
    echo "Por favor, preencha os campos obrigatórios vazios.";
    exit;
}

foreach( $_POST as $value ){
    if( stripos($value, 'Content-Type:') !== FALSE ){
        echo "Houve um problema com as informações inseridas.";
        exit;
    }
}

require 'phpmailer/PHPMailerAutoload.php';

//Criando uma nova instância PHPMailer
$mail = new PHPMailer;
$mail->Charset = "UTF-8";
  
if (!$mail->ValidateAddress($email)){
    echo "Por favor informe um email válido.";
    exit;
}

$mail->isSMTP();

$mail->Debugoutput = 'html';

$mail->Host = 'smtp.brunoalbano.com.br';

$mail->Port = 587;
$mail->IsHTML(true);
  
$mail->setFrom('materiais@meuspedidos.com.br', 'Meus Pedidos');

//Destino
$mail_receiver = $_POST['email'];
 
$mail->addAddress($mail_receiver, "");

$mail->addAttachment('ebook/livro-gestao-representantes.pdf');

//Assunto
$mail->Subject = utf8_decode("[Seu ebook chegou] Gestão de Representantes");

$body = file_get_contents("email/email.php");

$mail->MsgHTML($body);

//envia a mensagem e confere se tem algum erro.
if (!$mail->send()) {
    echo "Mailer Error: " . $mail->ErrorInfo;
} else {
    header("Location:sucesso.php");
    exit();
}
  
}

?>