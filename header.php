<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8">
	<title>Meus Pedidos - Gestão de Representantes Comerciais</title>
	<meta name="description" content="[Ebook grátis] Gestão de representantes comerciais - Descubra como conquistar o comprometimento e aumentar o desempenho do seu time de vendas.">
  <link rel="icon" href="favicon.ico" />
  <link rel='stylesheet' href='css/style.min.css'>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  
  <header class="mp-header">
    <div class="mp-container">
      <h2 class="mp-logo"><a href="https://meuspedidos.com.br/" title="Meus pedidos" target="_blank"><img src="imgs/mp.png" alt="Meus Pedidos"/></a></h2>
      <h1>eBook - Gestão de Representantes Comerciais</h1>
      <p class="mp-lead">Conquiste o comprometimento e aumente o desempenho do seu time de vendas</p>
    </div>
  </header>