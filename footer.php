  <footer class="mp-footer">
    <div class="mp-container">
      <a href="tel:" title="Ligue para nós!" class="mp-link-phone">(47) 3432-3755</a>
      <address class="mp-address"><img src="imgs/ico-brasil.png" alt="Brasil" class="mp-icon-image"/> Av. Rolf Wiest 277, Auri Plaza Garten (7º andar) - Bom Retiro - Joinville SC - CEP 89223-005</address>
      <nav id="mp-menu-footer" class="mp-menu">
        <a href="https://meuspedidos.com.br/termos-de-uso/" title="Termos de Serviço" target="_blank">Termos de Serviço</a>
        <a href="https://meuspedidos.com.br/termos-de-uso/" title="Política de Privacidade" target="_blank">Política de Privacidade</a>
      </nav>
    </div>
  </footer>
  <script src="js/plugins/jquery.min.js"></script>
  <script src="js/plugins/jquery.maskedinput.min.js"></script>
  <script src="js/main.js"></script>
</body>
</html>