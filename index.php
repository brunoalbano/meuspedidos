<?php include('header.php'); ?>
  <section class="mp-section">
    <div class="mp-container">
      <div class="mp-columns">
        <div class="mp-column mp-col-one">
          <div class="mp-columns">
            <div class="mp-column mp-col-two">
              <div class="mp-box-title">
                <h2>Descubra como ajudar seus representantes a venderem mais</h2>
              </div>
              <p>Representantes comerciais são responsáveis pelo próprio desempenho, é verdade!</p>
              <p>Mas não tenha dúvidas: os resultados fora da curva em vendas só surgem quando existe uma sinergia e uma colaboração bilateral entre representante comercial e indústria.</p> 
              <p>Neste ebook abordamos ações que você, gerente comercial da indústria ou distribuidora, pode fazer <strong>para ajudar o seu representante comercial a vender mais.</strong></p>
            </div>
            <div class="mp-column mp-col-three mp-align-center">
              <img src="imgs/mp-book.png" alt="Ebook - Gestão de representantes" class="mp-ebook-image"/>
            </div>
          </div>
        </div>
        <div class="mp-column mp-col-four">
          <div class="mp-box-title">
            <h2>Faça download grátis</h2>
            <p>Basta preencher o formulário</p>
          </div>
          <form method="post" action="enviar.php" class="mp-form">
            <input type="text" name="nome" placeholder="Nome" required/> 
            <input type="email" name="email" placeholder="Email" required/> 
            <input type="tel" name="telefone" placeholder="Telefone" class="mp-input-phone" required/>
            <input type="submit" name="receber-book" value="Receber Ebook" class="mp-button mp-button-default"/>
          </form>
        </div>
      </div>
    </div>
  </section>
<?php include('footer.php'); ?>
